const express = require('express');
var path = require('path');

const routes = express.Router();

routes.get('/', (req, res) => {
  return res.sendFile('index.html', { root: './' })
})

module.exports = routes;