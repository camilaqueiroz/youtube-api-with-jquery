const urlSearchYoutube = 'https://www.googleapis.com/youtube/v3/search'
const KEY_API = 'YOUR-KEY-API';
const valueInput = $('#search')
let listVideos = [];
const html = $('#trends');
let loading = $('#loader');


$('#buttonSearch').click((e) => {
  e.preventDefault();
  html.empty();
  listVideos = [];
  loading.addClass('show')
  getVideosApi()
})

function getVideosApi() {
  $.ajax({
    url: urlSearchYoutube + '?key=' + KEY_API + '&part=snippet&type=video&q=' + valueInput.val(),
    success: (response) => {
      response.items.forEach(video => {
        listVideos.push(video)
      });
      refreshCards();
      postVideosApi();
    },
    error: (err) => {
      throw new Error(err)
    }
  })
}
function postVideosApi() {
  $.ajax({
    type: 'POST',
    url: 'search.html',
    data: listVideos,
    success: (response) => {
      console.info(response);
    },
    error: (err) => {
      console.error(err);
    }
  })
}

function refreshCards() {
  if (listVideos.length > 0) {
    $('#statusSearch').html('');
    listVideos.forEach(video => {
      html.append('<div class="card"><div class="card-thumbnail"><img src=' + video.snippet.thumbnails.medium.url + ' alt="" /></div><div class="card-title"><a href="https://youtube.com/watch?v=' + video.id.videoId + '">' + video.snippet.title + '</a></div><div class="card-description"><p>' + video.snippet.description.substr(0, 20) + '</p><p>views</p><p>Publicado em: ' + video.snippet.publishedAt.substr(0, 7) + '</p></div></div> ')
    })
  } else {
    $('#statusSearch').html('<p>Não encontramos ' + valueInput.val() + '</p>')
  }
  loading.removeClass('show');
}